﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

using CoursesApp_Sec002.Data;
using CoursesApp_Sec002.Models;
using CoursesApp_Sec002.Models.ViewModels;
using Microsoft.AspNetCore.Http;

namespace CoursesApp_Sec002.Controllers
{
    public class EnrollmentController : Controller
    {
        private readonly CoursesContext _context;

        public EnrollmentController(CoursesContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            List<Enrollment> enrollment = _context.Enrollments
                .Include(e => e.Course)
                .Include(e => e.Student).ToList();

            return View(enrollment);
        }

        public IActionResult Create(int studentId)
        {
            Student student = _context.Students.SingleOrDefault(st => st.Id == studentId);
            if(student == null)
            {
                return NotFound();
            }
            List<Course> courses = _context.Courses.ToList();

            EnrollmentsCreateViewModel model = new EnrollmentsCreateViewModel();

            model.Student = student;
            model.Courses = courses;

            return View(model);
        }

        [HttpPost]
        public IActionResult Create(IFormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                Enrollment enrollment = new Enrollment();
                enrollment.StudentId = int.Parse(formCollection["StudentId"].ToString());

                //obtengo el titulo del curso al cual se selecciono
                string courseName = formCollection["Enrollment.CourseId"].ToString();

                //busco en la base de datos el curso que se selecciono
                Course course = _context.Courses.FirstOrDefault(c => c.Title.Equals(courseName));

                enrollment.EnrollmentDate = System.DateTime.Now;

                //le asigno el id del curso al enrolamiento
                enrollment.CourseId = course.Id;

                _context.Enrollments.Add(enrollment);
                _context.SaveChanges();
            }

            return Redirect("/student/");
        }
    }
}
