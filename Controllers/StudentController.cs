﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;

using CoursesApp_Sec002.Data;
using CoursesApp_Sec002.Models;
using Microsoft.EntityFrameworkCore;

namespace CoursesApp_Sec002.Controllers
{
    public class StudentController : Controller
    {
        private readonly CoursesContext _context;

        public StudentController(CoursesContext context){
            this._context = context;
        }

        public IActionResult Index()
        {
            List<Student> students = _context.Students.ToList();

            return View(students);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Student student)
        {
            if (ModelState.IsValid)
            {
                student.EnrollmentDate = System.DateTime.Now;
                _context.Students.Add(student);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        public IActionResult Edit(int studentId)
        {
            Student student = _context.Students.SingleOrDefault(x => x.Id == studentId);
            if(student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        [HttpPost]
        public IActionResult Edit(Student student)
        {
            if (ModelState.IsValid)
            {
                _context.Students.Update(student);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }
    }
}
