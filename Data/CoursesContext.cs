﻿using Microsoft.EntityFrameworkCore;
using CoursesApp_Sec002.Models;

namespace CoursesApp_Sec002.Data
{
    public class CoursesContext : DbContext
    {
        public CoursesContext(DbContextOptions<CoursesContext> options) : base(options){}

        public DbSet<Course> Courses { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }

    }
}
