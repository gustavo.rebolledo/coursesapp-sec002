﻿using System.ComponentModel.DataAnnotations;
using System;
namespace CoursesApp_Sec002.Models
{
    public class Enrollment
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int CourseId { get; set; }
        [Required]
        public int StudentId { get; set; }
        public Grade? Grade { get; set; }
        public DateTime EnrollmentDate { get; set; }

        public Course Course { get; set; }
        public Student Student { get; set; }
    }

    public enum Grade
    {
        A, B, C, D, E, F
    }
}
