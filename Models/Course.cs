﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace CoursesApp_Sec002.Models
{
    public class Course
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        [Required]
        [Range(1, 120)]
        public short Credits { get; set; }

        public ICollection<Enrollment> Enrollments { get; set; }

        public override string ToString()
        {
            return Title;
        }
    }
}
