﻿using System.Collections.Generic;

namespace CoursesApp_Sec002.Models.ViewModels
{
    public class EnrollmentsCreateViewModel
    {
        public List<Course> Courses { get; set; }
        public Student Student { get; set; }
        public Enrollment Enrollment { get; set; }
    }
}
